package com.myexample.loom;

import java.lang.FiberScope;

public class App
{
    private void foo() {
        System.out.println("Hello I'm foo.");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Exiting foo...");
    }

    private void bar() {
        System.out.println("Hello I'm bar.");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Exiting bar...");
    }

    void x() {
        //var deadline = Instant.now().plusSeconds(10);
        try (var scope1 = FiberScope.open()) {
            scope1.schedule(this::foo);
            scope1.schedule(this::bar);
        }
    }

    public static void main( String[] args )
    {
        App app = new App();
        app.x();
        System.out.println("End of Fiber scheduling!");
    }
}
